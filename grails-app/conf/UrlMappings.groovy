class UrlMappings {

  static mappings = {

    "/users/$id?"(controller: "users", parseRequest: true){
      action = [GET: "index", PUT: "update"]
    }

    "/profiles/$id?"(controller: "profiles", parseRequest: true){
      action = [GET: "index"]
    }

    "/jobs/$id?"(controller: "jobs", parseRequest: true){
      action = [GET: "index", POST: "save", PUT: "update"]
    }

    "/jobs/$jobId/sections/$sectionId?"(controller: "sections", parseRequest: true){
      action = [GET: "index"]
    }

    "/jobs/$jobId/questions/$questionId?"(controller: "questions", parseRequest: true){
      action = [GET: "index", POST: "save"]
    }

    "/jobs/$jobId/questions/$questionId/comments"(controller: "questions", parseRequest: true){
      action = [POST: "saveComment"]
    }

    // "/jobs"(controller:"jobs", parseRequest: true){
    //   action = [GET: "index"]
    // }

    // "/$controller/$action?/$id?"{
    //   constraints {
    //     // apply constraints here
    //   }
    // }

    "/"(controller: "welcome"){
      action = [GET: "index"]
    }

    "500"(view:'/error')
  }
}
