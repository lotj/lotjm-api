dataSource {
    pooled = true
    driverClassName = "com.mysql.jdbc.Driver"

    // DB Connection Pool (DBCP) properties
    properties {
      maxActive = 10
      maxIdle = 5
      minIdle = 2
      initialSize = 3
      minEvictableIdleTimeMillis = 60000
      timeBetweenEvictionRunsMillis = 60000
      maxWait = 10000
      validationQuery = "SELECT 1"
    }
}

hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = true
    cache.provider_class = 'net.sf.ehcache.hibernate.EhCacheProvider'
}

// environment specific settings
environments {

    development {
        dataSource {
            dbCreate = "validate"
            // the IP address for lotjm-server is defined on /etc/hosts
            url = "jdbc:mysql://lotjm-development-db/dtree"
            username = "grails"
            password = "grails"
        }
        hibernate {
          show_sql = true
        }

    }

    test {
        dataSource {
            driverClassName = "org.h2.Driver"
            dbCreate = "update"
            url = "jdbc:h2:mem:testDb;MVCC=TRUE;LOCK_TIMEOUT=10000"
            username = "sa"
            password = ""
        }
    }

    staging {
        dataSource {
            dbCreate = "validate"
            url = "jdbc:mysql://lotjm-test/dtree"
            username = "grails"
            password = "grails"
        }
    }

    production {
        dataSource {
            dbCreate = "validate"
            url = "jdbc:mysql://127.0.0.1/dtree"
            username = "grails"
            password = "grails"
        }
    }
}
