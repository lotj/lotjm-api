import api.*

class BootStrap {

    def init = { servletContext ->

      // Check serialization trick
      // from http://stackoverflow.com/a/12559158/483566

      // Per environment bootstrapping code
      environments {
        development {
        }
      }

    } // end init


    def destroy = {
    }
}
