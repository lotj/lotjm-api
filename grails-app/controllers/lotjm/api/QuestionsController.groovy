package lotjm.api

import api.*

import grails.converters.XML
import grails.converters.JSON

class QuestionsController {

  def applicationService
  def jobService
  def userService

  def getInfo(){
    ["GET /v1/jobs/3456/questions -- List all questions available on job #3456 given all previous answers; a traling question ID will give you full details of the question, including valid answers and comments.",
     "POST /v1/jobs/3456/questions/789 -- Make a selection of one or many answers on question #789; requires a param 'answer' with a list of valid answers",
     "POST /v1/jobs/3456/questions/789/comments -- add a new comment on question #789 on job #3456; requires 'text' field and a parentId is optional (if replying to an existing comment)"]
  }

  def index() {

    def login = request.getRemoteUser()
    def app = applicationService.getApplicationForLogin(login)
    def jobId = new Integer(params.jobId)
    def jobIds = jobService.jobListForUserAndApp(login, app.id)*.id
    def questions = []

    def output = []
    def status = 200

    if (jobIds.contains(jobId)){

      def job = Job.get(jobId)
      def sections = jobService.getRainbowPageData(jobId)

      // Always mark the first page of the first section as active, so the API
      // shows the first questions to start answering
      try { sections[0].pages[0].active = true } catch (Exception ex){ println ex }

      sections.each { section ->

        // find the ACTIVE pages of the section
        def pageIds = section.pages.findAll{it.active}*.id

        // find the questions of the section
        def sectQs = jobService.getQuestionsForAppProfileSection(app.id, job.profileId, section.id)

        // find non-comment questions in the ACTIVE pages, add them to the questions
        questions += sectQs.findAll {
          it.summary && it.summary != "Comments" && pageIds.contains(it.pageId)
        }

      }

      // If looking for a single question
      if (params.questionId){

        def q = questions.find{ it.questionId == new Integer(params.questionId) }

        if (q){
          // TODO retrieve answers and comments
          output = [id: q.questionId, summary: q.summary, body: q.body,
                    type: q.type, ddv: q.ddv]

          if (['mcq','checkbox'].contains(q.type)){
            output.answers = jobService.getAnswers(jobId, q.questionId)
          }

          def comments = jobService.getComments(jobId, q.questionId)
          if (comments){ output.comments = comments }

        } else {
          status = 404
          output = [developerMessage: "Bad question ID", userMessage: "Not found"]
        }

      } else {
        // no question ID param => list of the questions available so far
        output = []
        questions.each {
          output << [id: it.questionId, summary: it.summary]
        }
      }

    } else {
      status = 401
      output = [developerMessage: "User not authorized to access this job",
          userMessage: "You don't have the permissions to access the job"]
    }

    response.status = status
    withFormat{
      xml { render output as XML }
      json { render output as JSON }
    }

  }

  def save() {

    def login = request.getRemoteUser()
    def app = applicationService.getApplicationForLogin(login)

    def user = userService.userByAppAndLogin(app.id, login)
    def userId = user.id

    def jobId = new Integer(params.jobId)
    def jobIds = jobService.jobListForUserAndApp(login, app.id)*.id
    def questions = []

    def output = []
    def status = 200

    // verify that some answer IDs were sent
    if (!params.answer){
      status = 400
      output = [developerMessage: "Answer ID(s) missing", userMessage: "Bad request"]
    }

    if (status == 200 && jobIds.contains(jobId)){

      def job = Job.get(jobId)
      def sections = jobService.getRainbowPageData(jobId)

      try { sections[0].pages[0].active = true } catch (Exception ex){ println ex }

      sections.each { section ->
        def pageIds = section.pages.findAll{it.active}*.id
        def sectQs = jobService.getQuestionsForAppProfileSection(app.id, job.profileId, section.id)
        questions += sectQs.findAll {
          ['mcq','checkbox'].contains(it.type) &&
          it.summary && it.summary != "Comments" && pageIds.contains(it.pageId)
        }
      }

      def questionId = new Integer(params.questionId)
      def questionIds = questions*.questionId

      // Validate question ID
      if (!questionIds.contains(questionId)){
        status = 400
        output = [developerMessage: "Question is not multiple choice or checkbox",
                  userMessage: "Bad request"]
      }

      if (status == 200){
        // The question ID must be specified
        if (questionId){
          jobService.saveAnswer(userId, jobId, new Integer(params.questionId), params.answer)

          // Forward to the index to obtain the question details again
          forward( action: "index", params: [jobId: jobId, questionId: params.questionId])
          return

        } else {
          status = 400
          output = [developerMessage: "Question ID missing", userMessage: "Bad request"]
        }
      }

    }

    response.status = status
    withFormat{
      xml { render output as XML }
      json { render output as JSON }
    }

  }

  // POST /v1/jobs/3456/questions/456/comments ==> creates new comment
  def saveComment(){

    def login = request.getRemoteUser()
    def app = applicationService.getApplicationForLogin(login)

    def user = userService.userByAppAndLogin(app.id, login)
    def userId = user.id

    def jobId = new Integer(params.jobId)
    def jobIds = jobService.jobListForUserAndApp(login, app.id)*.id
    def questions = []

    def output = []
    def status = 200

    if (jobIds.contains(jobId)){
      def job = Job.get(jobId)

      // TODO validate that the job actually contains the question
      def parentId = 0
      try { parentId = new Integer(params.parentId) } catch (Exception ex){ }
      def questionId = new Integer(params.questionId)
      def text = params.text

      jobService.saveComment(userId, jobId, questionId, text, parentId)

      // Forward to the index to obtain the question details again
      forward( action: "index", params: [jobId: jobId, questionId: params.questionId])
      return

    } else {
      status = 401
      output = [developerMessage: "User not authorized to access this job",
          userMessage: "You don't have the permissions to access the job"]
    }



    output = [saved: true]

    response.status = status
    withFormat{
      xml { render output as XML }
      json { render output as JSON }
    }

  }

}
