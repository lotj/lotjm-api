package lotjm.api

import api.User

import java.text.SimpleDateFormat

import grails.converters.XML
import grails.converters.JSON

class UsersController {

  def applicationService
  def userService

  def getInfo(){
    ["GET /users -- Obtain the list of users with their ID, full name, login and email. With a trailing user ID obtains also the password (plain text), ID of a substitute and the Out Of Office start and end dates (if defined).",
     "PUT /users/2345 -- Updates the attributes of the user whose ID is 2345 (except the user ID). OOO dates and substitute ID can be set to null."]
  }


  def index = {
    def login = request.getRemoteUser()
    def application = applicationService.getApplicationForLogin(login)

    def output = null

    if (params.id){
      def uid = new Integer(params.id)
      def u = User.findByIdAndApplicationAndIsDisabled(uid, application, 0)
      output = [
        id: u.id, name: u.name, email: u.email, login: u.login,
        password: u.password, oooStart: u.oooStart, oooEnd: u.oooEnd,
        substituteUserId: u.substituteUserId
      ]
    } else {
      // If returning a list, show a few attributes only
      output = []
      userService.getActiveUsers(application.id).each { u ->
        output << [id: u.id, name: u.name, email: u.email, login: u.login]
      }
    }

    withFormat{
      xml { render output as XML }
      json { render output as JSON }
    }

  }

  // NOTE: it requires proper "Content-type: xxx/xxx" headers!
  def update = {
    def login = request.getRemoteUser()
    def application = applicationService.getApplicationForLogin(login)
    def uid = new Integer(params.id)

    def user = User.findByIdAndApplicationId(uid, application.id)

    // allow only a few fields to be updateable
    user.properties['name','password','email','disable','oooStart','oooEnd'] = params

    def sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")

    // OOO Start
    if (params.containsKey('oooStart')){
      if (params.oooStart.toString() == "null"){
        user.oooStart = null
      } else {
        try { user.oooStart = sdf.parse( params.oooStart ) }
        catch (Exception ex){ println ex }
      }
    }

    // OOO End
    if (params.containsKey('oooEnd')){
      if (params.oooEnd.toString() == "null"){
        user.oooEnd = null
      } else {
        try { user.oooEnd = sdf.parse( params.oooEnd ) }
        catch (Exception ex){ println ex }
      }
    }

    def output = null

    if (user.validate()) {
      def u = user.save()

      output = [
        id: u.id, name: u.name, email: u.email, login: u.login,
        password: u.password, oooStart: u.oooStart, oooEnd: u.oooEnd
      ]

    } else {
      response.status = 400 // "Error 400 - Bad Request" here
      output = [
        developerMesage: user.errors.fieldError.code,
        userMessage: user.errors.fieldError.defaultMessage
      ]
    }

    withFormat{
      xml { render output as XML }
      json { render output as JSON }
    }

  }

}
