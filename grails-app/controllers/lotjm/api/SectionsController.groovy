package lotjm.api

import api.*

import grails.converters.XML
import grails.converters.JSON
import java.text.SimpleDateFormat

class SectionsController {

  def applicationService
  def jobService

  def getInfo(){
    ["GET /v1/jobs/3456/sections -- List the sections available on job #3456 per the job profile, and if they are active or not. Trailing section ID is optional. If present, shows the list of QUESTIONS contained on the job 3456 for the specified section ID"]
  }

  def index() {

    def login = request.getRemoteUser()
    def app = applicationService.getApplicationForLogin(login)
    def jobId = new Integer(params.jobId)
    def jobIds = jobService.jobListForUserAndApp(login, app.id)*.id

    def output = null
    def status = 200

    if (jobIds.contains(jobId)){
      def job = Job.get(jobId)
      def sections = jobService.getRainbowPageData(jobId)

      if (params.sectionId){

        // list the questions in a given section
        def sectionId = new Integer(params.sectionId)

        def section = sections.find{ it.id == sectionId }
        if (section){

          def pageIds = section.pages*.id

          output = []

          def questions = jobService.getQuestionsForAppProfileSection(app.id, job.profileId, sectionId)
          questions.findAll{it.summary && it.summary != "Comments"}.each {
            output << [questionId: it.questionId, summary: it.summary]
          }

        } else {
          status = 404
          output = [developerMessage: "Bad section ID", userMessage: "Not found"]
        }

      } else {
        // no section ID param => list of the sections
        output = []
        sections.each{ section ->
          def active = section.pages*.active.contains(true)
          output << [id: section.id, name: section.name, active: active]
        }

        // mark the first section as active, always
        try { output[0].active = true } catch (Exception ex){ }

      }

    } else {
      status = 401
      output = [developerMessage: "User not authorized to access this job",
          userMessage: "You don't have the permissions to access the job"]
    }

    response.status = status
    withFormat{
      xml { render output as XML }
      json { render output as JSON }
    }

  }
}
