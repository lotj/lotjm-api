package lotjm.api

import api.*

import grails.converters.XML
import grails.converters.JSON
import java.text.SimpleDateFormat

class JobsController {

  def applicationService
  def jobService
  def userService

  def getInfo(){
    ["GET /jobs -- Obtain the list of jobs available to the user (trailing job ID optional).",
     "POST /jobs -- Create a new job; requires: name, profileId, approvalPath (list of userIDs separated by commas); optional: agencyName, description, jobNo, dueDate, telephoneNumber.",
     "PUT /jobs/3456 -- update the optional attributes (see POST) of the job #3456."]
  }

  def index() {

    def login = request.getRemoteUser()
    def app = applicationService.getApplicationForLogin(login)
    def sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")

    // find the jobs in the same application which are not archived
    def jobs = jobService.jobListForUserAndApp(login, app.id)
    def approvalTypes = ['sequential','simultaneous']

    def status = 200
    def output = []

    jobs.each { job ->
      def approvalPath = []
      job.approvers.each { a ->
        try {
          approvalPath << [id: a.user.id, name: a.user.name, order: a.order]
        } catch (Exception ex){
        }
      }

      def createdDate = null
      try { createdDate = sdf.format(job.createdDate) } catch (Exception ex){ }
      def dueDate = null
      try { dueDate = sdf.format(job.dueDate) } catch (Exception ex){ }

      def modifiedDate = null
      try { modifiedDate = sdf.format(job.modifiedDate) } catch (Exception ex){ }

      def item = [id: job.id, agencyName: job.agencyName, name: job.name,
                  createdDate: createdDate, jobNo: job.jobNo,
                  description: job.description, dueDate: dueDate,
                  isClosed: job.isClosed, createdId: job.createdUser.id,
                  modifiedDate: modifiedDate, ownerId: job.ownerUser.id,
                  profileId: job.profileId, telephone: job.telephone,
                  approvalType: approvalTypes[job.approvalType],
                  approvalPath: approvalPath]

      output << item
    }

    // Querying by name
    if (params.q){
      def Q = params.q.toUpperCase()
      output = output.findAll{ it.name.toUpperCase().contains(Q) }
    }

    // Ordering
    if (!params.ordering){ params.ordering = "desc" }
    if (!params.orderBy) { params.orderBy = "modifiedDate" }

    def ordering = params.ordering
    def orderBy = params.orderBy

    if (ordering == "desc"){
      output = output.sort{a,b -> b[orderBy] <=> a[orderBy] }
    } else {
      output = output.sort{a,b -> a[orderBy] <=> b[orderBy] }
    }

    if (params.id){
      try {
        def jobId = new Integer(params.id)
        output = output.findAll{ it.id == jobId }[0]
        output.id
      } catch (Exception ex){
        status = 404
        output = [developerMessage: "Not found", userMessage:"Job not found"]
      }
    }

    response.status = status
    withFormat{
      xml { render output as XML }
      json { render output as JSON }
    }

  }

  // http://grails.org/doc/latest/guide/single.html#REST
  def save(){
    def login = request.getRemoteUser()
    def app = applicationService.getApplicationForLogin(login)
    def user = User.findByApplicationAndLogin(app, login)

    def job = new Job()
    def output = null
    def status = 200

    // allow fields to be set
    job.properties['name','profileId','agencyName','description','jobNo'] = params

    // set values for these fields
    job.createdUser = user
    job.createdDate = new Date()
    job.modifiedDate = new Date()

    // TODO change this based on the approval path params
    job.ownerUser = user

    // default values for these fields if not set via params
    job.approvalType = ("simultaneous" == params.approvalType)? 1 : 0
    job.agencyName = params.agencyName?: ""
    job.description = params.description?: ""
    job.jobNo = params.jobNo?: ""
    job.telephone = params.telephone?: ""

    // TODO refactor this SDF
    def sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
    def approvalTypes = ['sequential','simultaneous']

    // profileId param validation
    def profiles = Profile.findAllByApplicationIdAndActive(app.id, true)*.categoryId
    if (!profiles.contains(params.profileId)){
      status = 400
      output = [developerMessage: "Invalid profile Id",
                userMessage: "Invalid profile specified"]
    }

    // APPROVAL PATH VALIDATIONS
    def approvalPath = params.approvalPath

    // Validation #1: the approval path should not be empty or null
    def passed = true
    try {
      if (!approvalPath || approvalPath.size() == 0){ passed = false }
      def approvalPathLen = approvalPath.findAll{ it != null && (it instanceof Integer) }.size()

      // there are garbage entries on the approvalPath
      if (approvalPath.size() != approvalPathLen){ passed = false }
    }
    catch (Exception ex){ passed = false }

    if (!passed){
      status = 400
      output = [developerMessage: "Approval path is missing or empty or not a list of integers",
                userMessage: "Invalid approval path"]
    }

    // Validation #2: the approval path should have at least two different entries
    passed = true
    try {
      def set = new HashSet()
      approvalPath.findAll{ it != null && (it instanceof Integer) }.each{ set << it }
      if (set.size() < 2){ passed = false }
    } catch (Exception ex){ passed = false }

    if (!passed){
      status = 400
      output = [developerMessage: "Approval path requires 2 or more different Integer IDs",
                userMessage: "Invalid approval path"]
    }

    // Validation #3: all path users should be active users of the same application
    passed = true
    try {
      def users = userService.getActiveUsers(app.id)*.id
      approvalPath.each { id ->
        if (!users.contains(id)){ passed = false }
      }
    } catch (Exception ex){ passed = false }

    if (!passed){
      status = 400
      output = [developerMessage: "Approval path includes bad user IDs",
                userMessage: "Invalid approval path"]
    }

    // Validation #4: a user should not be in two consecutive places of the path
    passed = true
    try {
      def pathAsString = approvalPath.join(",")
      approvalPath.each{ userId ->
        def badEntry = "${userId},${userId}".toString()
        if (pathAsString.indexOf(badEntry) > -1){ passed = false }
      }
    } catch (Exception ex){
      println ex
      passed = false }

    if (!passed){
      status = 400
      output = [developerMessage: "Approval path includes a user in two consecutive places",
                userMessage: "Invalid approval path"]
    }

    // TODO
    // IF the profile has 'jobOwnerAsFinalApprover' set to true
    // THEN the last approver of the path should be the same as the first one

    // TODO if the profile has 'forbiddenToExternalUsers' then all users
    // must not belong to a 'External Users' group

    // if (!validateApprovalPath(approvalPath)){
    //   status = 400
    //   output = [developerMessage: "Invalid approval path",
    //             userMessage: "Invalid approval path"]
    // }

    if (status == 200 && job.validate()){
      try {
        job.save()

        // approval path entries
        def i = 0
        for ( ; i < approvalPath.size() ; i++){
          def approver = new Approver(job: job, order: (i+1))
          approver.user = User.findById( approvalPath[i] )
          def managerId = 0
          if (i < approvalPath.size() - 1){ managerId = approvalPath[i+1] }
          approver.managerId = managerId
          approver.save()
        }

        // Now forward to the index passing the ID so it returns the new job
        forward( action: "index", params: [id: job.id])
        return

      } catch (Exception ex){
        log.warn ex

        status = 500
        output = [developerMessage: ex.getMessage,
                  userMessage: "Unexpected error on the server"]
      }
    } else {
      status = 400
      if (!output){
        output = [developerMesage: job.errors.fieldError.code,
                  userMessage: job.errors.fieldError.defaultMessage]
      }
    }

    // Final output here
    response.status = status
    withFormat{
      xml { render output as XML }
      json { render output as JSON }
    }

  }

  // PUT /v1/jobs/3456
  def update() {

    def login = request.getRemoteUser()
    def app = applicationService.getApplicationForLogin(login)
    def jobId = new Integer(params.id)
    def jobIds = jobService.jobListForUserAndApp(login, app.id)*.id

    def output = null
    def status = 200

    if (jobIds.contains(jobId)){

      def job = Job.get(jobId)

      // allow the following attributes to the updated
      job.properties['name','description','jobNo','agencyName','telephone'] = params

      // Due date
      def sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")

      if (params.containsKey('dueDate')){
        if (params.dueDate.toString() == "null"){
          job.dueDate = null
        } else {
          try { job.dueDate = sdf.parse( params.dueDate ) }
          catch (Exception ex){ println ex }
        }
      }

      if (job.validate()) {

        job.save()

        // TODO implement changes to the approval path, checking if the users
        // can be changed (eg. if they have not approved anything yet)

        forward( action: "index", params: [id: job.id])
        return

      } else {
        response.status = 400 // "Error 400 - Bad Request" here
        output = [
            developerMesage: user.errors.fieldError.code,
            userMessage: user.errors.fieldError.defaultMessage]
      }


    } else {
      status = 401
      output = [developerMessage: "User not authorized to access this job",
          userMessage: "You don't have the permissions to access the job"]
    }

    // Final output here
    response.status = status
    withFormat{
      xml { render output as XML }
      json { render output as JSON }
    }

  }

}
