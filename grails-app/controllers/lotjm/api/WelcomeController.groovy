package lotjm.api

import grails.converters.XML
import grails.converters.JSON
import java.text.SimpleDateFormat

class WelcomeController {

  // http://stackoverflow.com/a/4848546/483566
  def grailsUrlMappingsHolder

  def getInfo(){
    ["GET / -- Gives the developers a welcome message and help on the available functions"]
  }

  def index() {

    def sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")

    def output = [:]

    // developer info and welcome
    output.developerInfo = [
      applicationName: grailsApplication.getMetadata().getApplicationName(),
      applicationVersion: grailsApplication.getMetadata().getApplicationVersion(),
      description: "Welcome to the LOTJ Marketing web API. Valid functions are listed with the HTTP methods they use and a brief description of each one. For further assistance, ask tech@lotj.com",
      // jvmVersion: System.getProperty('java.version'),
      systemTime: sdf.format(new Date()),
      // quote: getQuote()
    ]

    def verbs = []

    // for each entry in grailsUrlMappingsHolder.urlMappings
    // obtain its list of actionName, logicalMappings

    def urlMappings = grailsUrlMappingsHolder.urlMappings
    urlMappings.findAll{ it.actionName != null }.each { mapping ->

      def methods = []
      def actions = mapping.actionName.each{ k, v -> methods << k }
      def urlPatterns = "${mapping.urlData.urlPattern}".toString()
      def info = null
      try {
        def cName = mapping.controllerName
        def controller = grailsApplication.controllerClasses.find{it.logicalPropertyName == cName}
        def clazzname = controller.getClazz().getName()
        info = grailsApplication.classLoader.loadClass(clazzname).newInstance().getInfo()

      } catch (Exception ex){ }

      if (info){ verbs << [path: urlPatterns, methods: methods, description: info] }
      else { verbs << [path: urlPatterns, methods: methods] }
    }

    output.functions = verbs

    withFormat{
      xml { render output as XML }
      json { render output as JSON }
    }

  }

  def getQuote(){

    def quotes = [
      "Computers are like bikinis. They save people a lot of guesswork. -- Sam Ewing",
      "The question of whether computers can think is like the question of whether submarines can swim. -- Edsger W. Dijkstra",
      "Never trust a computer you can't throw out a window. -- Steve Wozniak",
      "There are two major products that come out of Berkeley: LSD and UNIX.  We don't believe this to be a coincidence. -- Jeremy S. Anderson",
      "Optimism is an occupational hazard of programming; feedback is the treatment. -- Kent Beck",
      "There are only two kinds of programming languages: those people always bitch about and those nobody uses. -- Bjarne Stroustrup",
      "Saying that Java is nice because it works on all OSes is like saying that anal sex is nice because it works on all genders. -- Alanna on http://www.bash.org/?338364",
      "Software is like sex: It's better when it's free. -- Linus Torvalds",
      "There are two ways to write error-free programs; only the third one works. -- Alan J. Perlis",
      "C is quirky, flawed, and an enormous success. -- Dennis M. Ritchie",
      "...if you aren't, at any given time, scandalized by code you wrote five or even three years ago, you're not learning anywhere near enough. -- Nick Black",
      "I tried setting my hotmail password to 'penis'. It said my password wasn't long enough. :( -- Raven on http://www.bash.org/?136524"
    ]

    return quotes[(Math.random() * quotes.size()).intValue()]
  }

}
