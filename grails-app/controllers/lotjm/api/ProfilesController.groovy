package lotjm.api

import api.Profile

import grails.converters.XML
import grails.converters.JSON

class ProfilesController {

  def applicationService

  def getInfo() {
    ["GET /v1/profiles -- returns the list of profiles available to the user. A trailing profile ID is optional"]
  }

  def index() {
    def login = request.getRemoteUser()
    def app = applicationService.getApplicationForLogin(login)

    def output = null

    if (params.id) {
      def id = new Integer(params.id)
      def p = Profile.findByCategoryIdAndApplicationIdAndActive(id, app.id, true)

      if (p == null){
        response.status = 400
        output = [
          developerMessage: "invalid id", userMessage: "Unknown profile"
        ]
      } else {
        // when a valid ID is specified, return a map
        output = [id: p.categoryId, name: p.name, description: p.description]
      }

    } else {
      // list of maps
      output = []
      Profile.findAllByApplicationIdAndActive(app.id, true).each{ p ->
        output << [id: p.categoryId, name: p.name, description: p.description]
      } // end each
    }

    withFormat{
      xml { render output as XML }
      json { render output as JSON }
    }

  }

}
