package api

import api.*

import groovy.sql.Sql

class UserService {

  def getActiveUsers(Integer appId){
    User.findAll("FROM User AS u WHERE u.application.id = ? AND u.isDisabled = false", [appId])
  }

  def userById(Integer userId){
    User.get(userId)
  }

  def userByAppAndLogin(appId, login){
    Application app = Application.get(appId)
    User.findByApplicationAndLogin(app, login)
  }

}
