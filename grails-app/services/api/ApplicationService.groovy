package api

import groovy.sql.Sql

class ApplicationService {

  def dataSource

  def getApplicationForLogin(String login){
    def user = User.findByLogin(login)
    return Application.get(user.applicationId)
  }

  def getApplicationById(Integer appId){
    return Application.get(appId)
  }

}
