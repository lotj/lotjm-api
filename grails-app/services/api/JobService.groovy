package api

import api.*
import java.text.SimpleDateFormat
import groovy.sql.Sql

class JobService {

  def dataSource

  def jobListForUserAndApp(String login, Integer appId){

    def query = "" +
      " from Job AS j " +
      " where j.ownerUser.application.id = ? " +
      " and j.isArchived = false " +
      " order by j.modifiedDate desc"

      // TODO implement restrictions on user, groups, etc.

      def jobs = Job.findAll(query, [appId], [max:100])

      jobs.each { job ->
        job.approvers = Approver.findAllByJob(job)
      }

      return jobs
  }

  // TODO rewrite
  def getRainbowPageData(Integer jobId) {
    def sql = Sql.newInstance(dataSource)
    def job = Job.get(jobId)
    def appId = job.ownerUser.application.id

    def rows = []

    // this massive query comes from menu.jsp @ 532
    sql.eachRow("""
      SELECT question.question_id, question.summary, question.type,
        (results_index.result_id is not null or text is not null) as has_result,
        page.page_id ,  page.title ,page.section_id, section.name
      FROM base_cat_question, question
      LEFT JOIN results_index ON results_index.project_id = ${jobId}
      AND question.question_id = results_index.question_id
      LEFT JOIN comments ON question.question_id=comments.question_id
        AND results_index.project_id=comments.project_id
      INNER JOIN page ON question.page_id = page.page_id
        AND question.application_id=page.application_id
      INNER JOIN section ON page.section_id = section.section_id
        AND page.application_id=section.application_id
      WHERE question.application_id = ${appId}
      AND base_cat_question.base_cat_id IN
        (SELECT base_cat_id FROM project_info WHERE project_id = ${jobId})
      AND question.application_id = base_cat_question.application_id
      AND question.question_id = base_cat_question.question_id
      GROUP BY question.question_id ORDER BY question.question_id
    """){ row->

      def item = [
        questionId: row.question_id, summary: row.summary, type: row.type,
        hasResult: row.has_result, pageId: row.page_id, pageTitle: row.title,
        sectionId: row.section_id, sectionName: row.name
      ]

      rows << item
    }

    // iterate through rows to find section names
    def sections = [:]
    rows.each { row ->

      def section = null
      if (sections.containsKey(row.sectionId)){
        section = sections[ row.sectionId ]
      } else {
        section = [id: row.sectionId, name: row.sectionName, pages: [:]]
        sections[row.sectionId] = section
      }

      def page = null
      if (section.pages.containsKey(row.pageId)){
        page = section.pages[row.pageId]
      } else {
        page = [:]
        def pages = section.pages
        pages[row.pageId] = page
        page.name = row.pageTitle
        page.id = row.pageId
        page.active = false
      }


      // if the page has a result or a comment, set it active
      if (row.hasResult) {
        page['active'] = true
      } else {
        // if it's a text-type question and the summary is not blank or "comments"
        // and has any comment, then it's answered too

        if (row.type == 'text' && row.summary?.trim().size() > 0 &&
            row.summary?.trim().toUpperCase() != 'COMMENTS'){
          // check for comments for this job and questionId
          def total = sql.firstRow("""
            SELECT COUNT(*) total FROM comments WHERE project_id = ${jobId}
            AND question_id = ${row.questionId}""").total
          if (total > 0) { page['active'] = true }
        } else {
          // mcq/checkbox questions too...
          def total = sql.firstRow("""
            SELECT COUNT(*) total FROM comments WHERE project_id = ${jobId}
            AND question_id = ${row.questionId}""").total
          if (total > 0) { page['active'] = true }
        }
      }
    }

    // sort everything
    def newSections = sections.values().toList().sort{a,b -> a.id <=> b.id }
    newSections.each { section ->
      def pages = section.pages.values().toList().sort{a,b -> a.id <=> b.id }
      section.pages = pages
    }

    // return ordered data
    return newSections
  }

  def getQuestionsForAppProfileSection(appId, profileId, sectionId){
    def sql = Sql.newInstance(dataSource)

    def output = []

    // TODO implement using models
    sql.eachRow("""
      SELECT bcq.question_id
      FROM base_cat_question bcq, question q, page p, section s
      WHERE bcq.application_id = ?
      AND bcq.base_cat_id = ?
      AND bcq.question_id = q.question_id
      AND q.application_id = bcq.application_id
      AND q.page_id = p.page_id
      AND p.application_id = bcq.application_id
      AND p.section_id = s.section_id
      AND s.application_id = bcq.application_id
      AND s.section_id = ?
    """, [appId, profileId, sectionId]){ row ->
      output << Question.findByApplicationIdAndQuestionId(appId, row.question_id)
    }

    return output

  }

  def getAnswers(jobId, questionId){
    def sql = Sql.newInstance(dataSource)
    def job = Job.get(jobId)
    def appId = job.ownerUser.application.id
    def sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")

    def output = [:]
    def selected = null

    // get the answers
    sql.eachRow("""
      SELECT * FROM question_response WHERE application_id=? AND question_id=?
    """,[appId, questionId]){ row ->
      def answer = [id: row.question_response_id, body: row.body,
                    feedback: row.feedback, information: row.dropdown]

      output[answer.id] = answer
    }

    // get recorded answers
    sql.eachRow("""
        SELECT * FROM results_index WHERE project_id = ? AND question_id = ?
        ORDER BY project_modification_time DESC LIMIT 1
    """, [job.id, questionId]){ row ->
      def timestamp = sdf.format(row.project_modification_time)
      output[row.result_id].selected = [userId: row.user_id, timestamp: timestamp]
    }

    return output.values()
  }

  def getComments(jobId, questionId){
    def sql = Sql.newInstance(dataSource)
    def job = Job.get(jobId)
    def appId = job.ownerUser.application.id
    def sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")

    def output = []

    def qType = sql.firstRow("""
      SELECT type FROM question WHERE application_id = ${appId}
      AND question_id = ${questionId}""").type

    // Adjust question number if the question is of type MCQ or checklist
    if (['mcq','checklist'].contains(qType)){ questionId++ }

    sql.eachRow("""
      SELECT * FROM comments WHERE project_id = ? AND question_id = ?
    """, [job.id, questionId]){ row ->

      def timestamp = sdf.format(row.comment_timestamp)
      output << [id: row.comment_id, userId: row.user_id, parentId: row.parent_id,
                 timestamp: timestamp, text: row.text]
    }

    return output

  }

  def saveAnswer(userId, jobId, questionId, answerId){
    def sql = Sql.newInstance(dataSource)
    def job = Job.get(jobId)
    def appId = job.ownerUser.application.id

    try {
      sql.execute("""
        INSERT INTO results_index
        (project_id, question_id, project_modification_time, result_id,
         user_id, is_modified, open, parent_id)
        VALUES (?, ?, NOW(), ?, ?, 1, 1, 0)
      """, [jobId, questionId, answerId, userId])
    } catch (Exception ex){ }

  }

  def saveComment(userId, jobId, questionId, text, parentId){
    def sql = Sql.newInstance(dataSource)
    def job = Job.get(jobId)
    def appId = job.ownerUser.application.id

    def qType = sql.firstRow("""
      SELECT type FROM question WHERE application_id = ${appId}
      AND question_id = ${questionId}""").type

    // Adjust question number if the question is of type MCQ or checklist
    if (['mcq','checklist'].contains(qType)){ questionId++ }

    sql.execute("""
      INSERT INTO comments
      (project_id, question_id, comment_timestamp, comment_id, user_id, open,
       parent_id, text, task_id, task_stage)
      VALUES (?, ?, NOW(), NULL, ?, 1, ?, ?, 0, 0)
    """, [jobId, questionId, userId, parentId, text])

  }

}
