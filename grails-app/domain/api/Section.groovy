package api

import org.apache.commons.lang.builder.HashCodeBuilder

class Section implements Serializable {

  Integer applicationId
  Integer sectionId
  String name
  boolean active

  static mapping = {
    table 'section'

    id composite: ['applicationId', 'sectionId']
    applicationId column: 'application_id'
    sectionId column: 'section_id'
    active column: 'status', type: 'numeric_boolean'
  }

  static constraints = {
    name blank: false
    applicationId min: 1
    sectionId min: 1
  }

  // REQUIRED BECAUSE OF THE COMPOSITE ID: hashCode() and equals()

  boolean equals(other){
    if (!(other instanceof Section)){
      return false
    }

    other.applicationId == applicationId && other.sectionId == sectionId
  }

  int hashCode(){
    def builder = new HashCodeBuilder()
    builder.append applicationId
    builder.append sectionId
    builder.toHashCode()
  }

  String toString(){
    return "api.Section: (app ${applicationId}, section ${sectionId})".toString()
  }


}
