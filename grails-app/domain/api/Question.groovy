package api

import org.apache.commons.lang.builder.HashCodeBuilder

class Question implements Serializable {

  Integer applicationId
  Integer questionId
  Integer pageId
  String type
  String body
  String summary
  String ddv
  boolean ddvOpen

  static mapping = {
    table 'question'

    id composite: ['applicationId', 'questionId']
    applicationId column: 'application_id'
    questionId column: 'question_id'
    pageId column: 'page_id'
    type column: 'type'

    body column: 'body', sqlType: 'text'
    summary column: 'summary', sqlType: 'text'
    ddv column: 'information', sqlType: 'text'

    ddvOpen column: 'information_open', type: 'numeric_boolean'
  }

  static constraints = {
  }

  // REQUIRED BECAUSE OF THE COMPOSITE ID: hashCode() and equals()

  boolean equals(other){
    if (!(other instanceof Question)){
      return false
    }

    other.applicationId == applicationId && other.questionId == questionId
  }

  int hashCode(){
    def builder = new HashCodeBuilder()
    builder.append applicationId
    builder.append questionId
    builder.toHashCode()
  }

  String toString(){
    return "api.Question: (app ${applicationId}, question ${questionId})".toString()
  }

}
