package api

import org.apache.commons.lang.builder.HashCodeBuilder

class Approver implements Serializable {

  // The PK of PROJECT_APPROVAL_SETUP is composed of PROJECT_ID and ORDER_ID
  // thus this class requires a composite primary key as described in
  // http://grails.org/doc/latest/guide/single.html#compositePrimaryKeys

  Job job
  Integer order

  User user
  Integer managerId
  boolean isSimultApproved

  static mapping = {
    table 'project_approval_setup'

    id composite: ['job', 'order']

    job column: 'project_id'
    order column: 'order_id'

    user column: 'user_id'
    managerId column: 'manager_id'

    // this field flags if the job is set for SIMULTANEOUS approvals AND
    // the user HAS APPROVED the job already

    isSimultApproved column: 'simultaneous_approval_flag',
                     type: 'numeric_boolean'
  }

  static constraints = {

    // The job owner and user must belong to the same Application
    job nullable: false, validator: { val, obj ->
      obj.job.ownerUser.application.id == obj.user.application.id
    }

    // The user and the manager should not be the same user ID
    user nullable: false, validator: { val, obj ->
      obj.user.id != obj.managerId
    }

    // The manager can be either 0 OR one of the users of the same Application
    managerId nullable: false, validator: { val, obj ->
      obj.managerId >= 0

      // TODO: Implement
      // (obj.managerId == 0) ||
      // (User.get( val ).application.id == obj.user.application.id)
    }

  }

  // REQUIRED BECAUSE OF THE COMPOSITE ID: hashCode() and equals()

  boolean equals(other){
    if (!(other instanceof Approver)){
      return false
    }

    other.job.id == job.id && other.order == order
  }

  int hashCode(){
    def builder = new HashCodeBuilder()
    builder.append job.id
    builder.append order
    builder.toHashCode()
  }

  String toString(){
    return "api.Approver: (job ${job.id}, order ${order})".toString()
  }

}
