package api

class Application {

  Integer id
  String name
  String description
  String logo
  String canonicalName

  static mapping = {
    table 'application_info'
    id column: 'application_id'
    canonicalName column: 'canonical_name'
  }

  static constraints = {
    // all of the properties are not null by default
    name blank: false
    description blank: false
    logo blank: false
    canonicalName blank: false
  }

}
