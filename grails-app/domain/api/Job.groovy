package api

class Job {

  Integer id
  User createdUser
  String name
  String description
  String jobNo
  User ownerUser
  String agencyName
  String telephone
  Integer profileId
  Integer approvalType
  Date createdDate
  Date modifiedDate
  Date dueDate

  boolean isArchived
  boolean isClosed

  transient List approvers

  static mapping = {
    table 'project_info'
    id column: 'project_id'
    jobNo column: 'job_no'
    profileId column: 'base_cat_id'
    approvalType: column: 'approval_type'

    // http://grails.org/doc/latest/guide/single.html#customHibernateTypes
    description sqlType: 'text'

    createdDate column: 'creation_time'
    modifiedDate column: 'modification_time'
    dueDate column: 'due_date'
    agencyName column: 'client_name'
    telephone column: 'owner_phone'

    // Coerce INTEGER into the primitive 'boolean' - NOT the Boolean class
    isArchived column: 'is_archived', type: 'numeric_boolean'
    isClosed column: 'stage_id', type: 'numeric_boolean'

    createdUser column: 'user_id'
    ownerUser column: 'owner_id'
  }

  static constraints = {
    createdUser blank: false
    name size: 3..64
    description blank: true, nullable: true
    jobNo blank: true, nullable: true
    profileId blank: false, min: 1
    approvalType blank:false, min: 0, max: 1
    agencyName blank: true, nullable: true
    telephone blank: true, nullable: true
    dueDate nullable:true
  }
}
