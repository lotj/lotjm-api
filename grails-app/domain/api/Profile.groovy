package api

import org.apache.commons.lang.builder.HashCodeBuilder

class Profile implements Serializable {

  Integer applicationId
  Integer categoryId
  String name
  String description
  boolean active
  boolean jobOwnerAsFinalApprover
  boolean forbiddenToExternalUsers

  static mapping = {
    table 'base_category'

    id composite: ['applicationId', 'categoryId']
    applicationId column: 'application_id'
    categoryId column: 'base_cat_id'

    active column: 'active', type: 'numeric_boolean'
    jobOwnerAsFinalApprover column: 'job_owner_final_approver',
                            type: 'numeric_boolean'

    forbiddenToExternalUsers column: 'disable_external_users',
                             type: 'numeric_boolean'
  }

  static constraints = {
    name blank: false
    categoryId min: 1
  }

  // REQUIRED BECAUSE OF THE COMPOSITE ID: hashCode() and equals()

  boolean equals(other){
    if (!(other instanceof Profile)){
      return false
    }

    other.applicationId == applicationId && other.categoryId == categoryId
  }

  int hashCode(){
    def builder = new HashCodeBuilder()
    builder.append applicationId
    builder.append categoryId
    builder.toHashCode()
  }

  String toString(){
    return "api.Profile: (app ${applicationId}, profile ${categoryId})".toString()
  }

}
