package api

class User {

  Integer id
  String name
  String login
  String password
  String email
  Application application
  boolean isDisabled
  Integer substituteUserId
  Date oooStart
  Date oooEnd

  static mapping = {
    table 'user_info'
    id column: 'user_id'
    password column: 'userpassword'
    application column: 'application_id'
    oooStart column: 'out_start_date'
    oooEnd column: 'out_end_date'
    substituteUserId column: 'substitute_user'

    isDisabled column: 'disable', type: 'numeric_boolean'
  }

  static constraints = {
    name blank: false
    login blank: false
    password blank: false
    email blank: false, email: true

    // OutOfOffice custom validation: oooStart and oooEnd can be null,
    // but if both exist, oooStart must be before oooEnd

    oooStart nullable: true, validator: { val, obj ->
      obj.oooEnd == null ||
      obj.oooStart == null ||
      obj.oooStart.before(obj.oooEnd)
    }
    oooEnd nullable: true, validator: { val, obj ->
      obj.oooEnd == null ||
      obj.oooStart == null ||
      obj.oooStart.before(obj.oooEnd)
    }

    // An user may have not selected a substitute yet
    substituteUserId nullable: true
  }

  // Model utils

  boolean outOfOffice() {
    oooStart &&
    oooStart.before(new Date()) &&
    (oooEnd == null || (new Date()+1).before(oooEnd))
  }

}
