package api



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(JobService)
class JobServiceTests {

    void testSomething() {
        fail "Implement me"
    }
}
