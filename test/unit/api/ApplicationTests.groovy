package api

import grails.test.mixin.*
import org.junit.*

@TestFor(Application)
class ApplicationTests {

  void testNullConstraints() {
    def app = new Application()
    mockForConstraintsTests(Application, [app])

    assert !app.validate()
    assert "nullable" == app.errors["name"]
    assert "nullable" == app.errors["description"]
    assert "nullable" == app.errors["logo"]
    assert "nullable" == app.errors["canonicalName"]
  }

  void testBlankConstraints() {
    def app = new Application(name:"", description:"", logo:"", canonicalName:"")
    mockForConstraintsTests(Application, [app])

    assert !app.validate()
    assert "blank" == app.errors["name"]
    assert "blank" == app.errors["description"]
    assert "blank" == app.errors["logo"]
    assert "blank" == app.errors["canonicalName"]
  }


}
