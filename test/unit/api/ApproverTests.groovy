package api

import grails.test.mixin.*
import org.junit.*

@TestFor(Approver)
class ApproverTests {

  void testNullConstraints(){

    def newApprover = new Approver()

    mockForConstraintsTests(Approver, [newApprover])

    assert !newApprover.validate()
    assert "nullable" == newApprover.errors["job"]
    assert "nullable" == newApprover.errors["user"]
    assert "nullable" == newApprover.errors["managerId"]
    assert "nullable" == newApprover.errors["order"]
  }


  void testCustomValidations(){
    // The job owner and user must belong to the same Application

    def app1 = new Application(id: 1, name:"1", description:"1", logo:"1",
                                   canonicalName:"1")
    def app2 = new Application(id: 2, name:"2", description:"2", logo:"2",
                                   canonicalName:"2")
    def user1 = new User(application: app1, name:"u1", login:"u1", password:"1",
                         email:"example1@example.org")
    def user2 = new User(application: app2, name:"u2", login:"u2", password:"2",
                         email:"example2@example.org")

    def job = new Job(ownerUser: user1)

    def appr1 = new Approver(job: job, user: user2, managerId: user1.id, order:1)
    def appr2 = new Approver(job: job, user: user1, managerId: user1.id, order:1)
    def appr3 = new Approver(job: job, user: user1, managerId: -1, order:1)
    def valid = new Approver(job: job, user: user1, managerId: 0, order:1)

    mockForConstraintsTests(Approver, [appr1, appr2, appr3])

    assert !appr1.validate()
    assert !appr2.validate()
    assert !appr3.validate()

    // job.ownerUser.application != user.application
    assert appr1.errors["job"] != null

    // user.id == managerId
    assert appr2.errors["user"] != null

    // manager not in the same application as the user
    assert appr3.errors["managerId"] != null

    // valid
    assert valid.validate()
  }

}
