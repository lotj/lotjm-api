package api

import grails.test.mixin.*
import org.junit.*

@TestFor(User)
class UserTests {

  void testConstraints() {
    def newUser = new User()
    def invalidEmailUser = new User(email: 'invalid@c')
    def validEmailUser = new User(email: 'user@example.com')
    mockForConstraintsTests(User, [newUser, invalidEmailUser, validEmailUser])

    assert !newUser.validate()
    assert "nullable" == newUser.errors["name"]
    assert "nullable" == newUser.errors["login"]
    assert "nullable" == newUser.errors["password"]
    assert "nullable" == newUser.errors["email"]

    assert !invalidEmailUser.validate()
    assert "email" == invalidEmailUser.errors["email"]

    assert !validEmailUser.validate()
    assert validEmailUser.errors["email"] == null
  }

  /*
    // OutOfOffice custom validation: oooStart and oooEnd can be null,
    // but if both exist, oooStart must be before oooEnd

    oooStart nullable: true, validator: { val, obj ->
      obj.oooEnd == null ||
      obj.oooStart == null ||
      obj.oooStart.before(obj.oooEnd)
    }
    oooEnd nullable: true, validator: { val, obj ->
      obj.oooEnd == null ||
      obj.oooStart == null ||
      obj.oooStart.before(obj.oooEnd)
    }
   */

  void testOutOfOffice(){

    def newUser = new User() // no OOO dates
    def oooStartUser = new User(oooStart: new Date())
    def oooEndUser = new User(oooEnd: new Date())

    // invalid because end date is 2 days before start date
    def oooInvalidUser = new User(oooStart: new Date(), oooEnd:(new Date() - 2))

    mockForConstraintsTests(User,
                            [newUser, oooStartUser, oooEndUser, oooInvalidUser])

    assert newUser.errors["oooStart"] == null
    assert newUser.errors["oooEnd"] == null

    assert oooStartUser.errors["oooStart"] == null
    assert oooStartUser.errors["oooEnd"] == null

    assert oooEndUser.errors["oooStart"] == null
    assert oooEndUser.errors["oooEnd"] == null

    assert !oooInvalidUser.validate()
    assert oooInvalidUser.errors["oooStart"] != null
    assert oooInvalidUser.errors["oooEnd"] != null
  }
}
