package api

import grails.test.mixin.*
import org.junit.*

@TestFor(Profile)
class ProfileTests {

  void testConstraints() {

    def profileWithoutName = new Profile(description:"")
    def profileWithoutApp = new Profile(name: "no app", description:"")

    def validProfile = new Profile(name: "valid", applicationId: 1,
        categoryId: 1, description:"")

    mockForConstraintsTests(Profile,
        [profileWithoutName, profileWithoutApp, validProfile])

    assert !profileWithoutName.validate()
    assert "nullable" == profileWithoutName.errors["name"]

    assert !profileWithoutApp.validate()
    assert "nullable" == profileWithoutApp.errors["applicationId"]

    assert validProfile.validate()
  }

}
