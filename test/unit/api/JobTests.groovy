package api

import grails.test.mixin.*
import org.junit.*

@TestFor(Job)
class JobTests {

  void testConstraints() {
    def job = new Job()

    mockForConstraintsTests(Job, [job])

    assert !job.validate()
    assert "nullable" == job.errors["name"]
    assert "nullable" == job.errors["profileId"]
    assert "nullable" == job.errors["approvalType"]

  }

}
