RESTful web API to access LOTJm
===============================

Built on Grails 2.1 using simple controllers which map to resources,
as explained in https://vimeo.com/17785736 (16 minutes)

The TL;DR version:

* Be RESTful
* Only 2 URLs [per resource: 1 for collections, 1 for individual elements]
  * /dogs      [POST, GET, DELETE]
  * /dogs/1234 [GET, PUT, DELETE]
* No verbs
* Use nouns as plurals
* Sweep complexity behind the '?'
* Borrow from leading APIs

There's an updated, expanded version now at:

http://blog.apigee.com/detail/slides_for_restful_api_design_second_edition_webinar/


